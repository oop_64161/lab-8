public class Rectangle { // Attributes
    private String name;
    private int weight;
    private int height;

    public Rectangle(String name, int weight, int height) { // Constuctor
        this.name = name;
        this.weight = weight;
        this.height = height;
    }

    public int printAreaRectangle() {
        int Area = weight * height;
        System.out.println(name + " area = " + Area);
        return Area;
    }

    public int printPerimeterRectangle() {
        int perimeter = (weight + height) * 2;
        System.out.println(name + " perimeter = " + perimeter);
        return perimeter;
    }
}
