public class Triangle {
    private String name;
    private int a;
    private int b;
    private int c;

    public Triangle(String name, int a, int b, int c) {
        this.name = name;
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public int printAreaTriangle() {
        double s = (a + b + c) / 2;
        double area = Math.sqrt(s * (s - a) * (s - b) * (s - c));
        System.out.println(name + " area = " + area);
        return (int) area;
    }

    public int printPerimeterTriangle() {
        int perimeter = a + b + c;
        System.out.println(name + " perimeter = " + perimeter);
        return perimeter;
    }
}
