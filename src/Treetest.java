public class Treetest { // Attributes
    private String name;
    private char symbol;
    private int x;
    private int y;

    public Treetest(String name, char symbol, int x, int y) { //Constructor
        this.name = name;
        this.symbol = symbol;
        this.x = x;
        this.y = y;
    }

    public void printTree() {
        System.out.println("Name: " + name);
        System.out.println("Symbol: " + symbol);
        System.out.println("Position x: " + x + " Position y: " + y);
    }
}
