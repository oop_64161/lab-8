public class CircleApp {
    public static void main(String[] args) {
        Circle circle1 = new Circle("Circle 1", 1);
        Circle circle2 = new Circle("Circle 2", 2);
        circle1.printAreaCircle();
        circle2.printAreaCircle();
        circle1.printPerimeterCircle();
        circle2.printPerimeterCircle();
    }
}
