public class Circle { // Attributes
    private String name;
    private int r;

    public Circle(String name, int r) {
        this.name = name;
        this.r = r;
    }

    public int printAreaCircle() {
        double area = 3.14 * (r * r);
        System.out.println(name + " area = " + area);
        return (int) area;
    }

    public double printPerimeterCircle() {
        double perimeter = 2 * 3.14 * r;
        System.out.println(name + " perimeter = " + perimeter);
        return perimeter;
    }
}
