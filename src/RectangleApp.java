public class RectangleApp {
    public static void main(String[] args) {
        Rectangle react1 = new Rectangle("Rectangle 1", 10, 5);
        Rectangle react2 = new Rectangle("Rectangle 2", 5, 3);
        react1.printAreaRectangle();
        react2.printAreaRectangle();
        react1.printPerimeterRectangle();
        react2.printPerimeterRectangle();
    }
}
