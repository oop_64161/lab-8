import java.net.NetPermission;

public class MapApp {
   public static void main(String[] args) {
    Map map1 = new Map();
    map1.printMap();
    Map map2 = new Map(10, 10);
    map2.printMap();
   }
}
